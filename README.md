## Vault Commands ##

This project contains the bash scripts utilized to allow for secure, encrypted
storage and retrieval of VPN password/connection, JWT, and/or other sensitive
data that can then be utilized for automated or manual tasks.

## Requirements ##

Hashicorp Vault installed (https://developer.hashicorp.com/vault/docs/install)

NOTE: If you are running the workstation container found here:
code.vt.edu:5005/it-common-platform/public-images/workstation
then Vault is already installed in the container as needed.


## VPN USAGE ##

Dependencies
------------

- openconnect installed on target system

How to use
==========

Must be connected to All traffic vpn or on a VT network directly for JWT. VPN usage can
be skipped if you alread are on the All traffic VPN or on a VT network directly.

If you are in a container, you can use the vpn connection option(make sure your workstation
is connected to the VT traffic vpn, as it would be required for accessing vault)

NOTE: If you have not yet stored your network password in Vault, you will first need to run:

> vault kv put secret/${USER}/network vpn="<network-password>"

You can obtain your network-password from https://accounts.it.vt.edu by clicking on "Get your wifi password" .

In the container, ensure you are in the shell in the container as your username that matches your
VT PID, then run the command:
> source vvpn.sh

Then:
> vvpn_connect

After successful vpn connection is established, you will be able to run the vjwt commands successfully.



## JWT USAGE ##

Dependencies
------------

- jq installed on target system

How to use
==========

Run the command:
> source vjwt.sh

Then, to create a new JWT if one is not currently available or has expired, and/or to print the password to stdout:

> vjwt_getpassword

From outside the workstation, on your local workstation environment, to store the JWT password in the clipboard:

> vjwt_copy

NOTE: Inside the container, the clipboard for your workstation cannot be accessed. As such, you cannot use the
vjwt_copy command from within the container. However, if default locations are utilized in the container and
workstation itself, then the vault token set at initial login will be functional from both environments with no
need to change any settings.

FIXME: At this point, the vjwt_copy command is hardcoded to use pbcopy, which is a Mac OS utility for copying to clipboard
from the terminal. This needs to be updated to check for various clipboard copying utilities to allow for other platforms.
For now, users can search the vjwt.sh script and replace the "pbcopy" command with their favorite clipboard command.


## AWS USAGE ##

Dependencies
------------

- awscli installed on target system
- jq installed on target system

How to use
==========

The client allows the AWS STS timeout variable to be set.

AWS_STS_TIMEOUT

  The length of time in seconds the token will be valid for. (default 900, optional)

The user needs to have Vault secrets per AWS account located at secret/${USER}/aws/<awsAccountName-or-Number> with fields of "id" and "key" that contain the AWS_ACCESS_KEY_ID and AWS_SECRET_ACCESS_KEY, respectively(retrieved from AWS).

> source vaws.sh

The Vault secrets should be created with the following commands:

```
    vault kv put secret/${USER}/aws/vtnis-ss id="<id>" key="<key>"
    ## replacing <id> and <key> with the acquired AWS_ACCESS_KEY_ID and AWS_SECRET_ACCESS_KEY.
```

To retrieve a token:

```
    vaws_sts_set <master-account> <account-to-be-managed> <role-name>
    ## Example utilizing vtnis-ss master account key, 541585145005 for the sts assume role account, and NISAdmin for the assumed role
    ## vaws_sts_set vtnis-ss 541585145005 NISAdmin
```

After execution you will have three environment variables set for use with the awscli, terraform, or other tool  utilizing the specified AWS Role.

To run adhoc AWS access key/id authenticated commands with tools such as awscli or terraform:

```
  ## List ec2 instances in the us-east-1 region in the account
  aws_do <awsAccountName-or-Number> aws ec2 describe-instances --region us-east-1
  ## Run terraform plan against the account
  aws_do <awsAccountName-or-Number> terraform plan
```

To run adhoc AWS commands with tools such as awscli or terraform utilizing temporary STS tokens:

```
  ## Run terraform plan against the target account, using STS authorized token generated from the shared services account, using the target assumed role
  aws_sts_do <awsAccountName-or-Number-of-STS-authorized-account> <target-awsAccountNumber> <AWS-assume-role-name> terraform plan
  ## Run terraform plan against the account with the number matching the current working directory, using STS authorized token generated from the shared services account(vtnis-ss in this case), using the NISAdmin assumed role.
  aws_sts_do vtnis-ss ${PWD##*/}  NISAdmin terraform apply
```
