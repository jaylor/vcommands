#!/bin/bash

short-jwt() {

    if [ "$#" -lt 1 ] || [ "$#" -gt 2 ]; then
        echo "Usage: $0 <exp:get:pass> <length-in-minutes>"
        exit 1
    fi
    
    if [ $1 == "get" ] && [ "$#" != 2 ]; then
      echo "Usage: $0 <exp:get:pass> <length-in-minutes>"
      exit 1
    fi
    
    # get jwt
    if [ $1 == "get" ] && [ "$#" -eq 2 ]; then
      minutes_ago=$(date -d "now - $2 minutes" +%s)
      if [ -f ~/.short-jwt ]; then
        echo ".short-jwt file found. setting file_time to file timestamp"
        file_time=$(date -r ~/.short-jwt +%s)
      else
        echo ".short-jwt file not found. setting file_time to $2 minutes ago"
        file_time=$(date -d "now - $2 minutes" +%s)
      fi
      if (( file_time <= minutes_ago )); then
        echo "Getting JWT!"
        curl -s "https://apps.middleware.vt.edu/ws/v2/accounts/tokens?validity=PT$2M" -XPOST -u "${JWT_PID:-$USER}" > ~/.short-jwt
      else
        echo "Full JWT:"
        cat ~/.short-jwt
        echo ""
      fi
    fi
    
    # display jwt pass
    if [ $1 == "pass" ]; then
      echo "JWT Pass:"
      cat ~/.short-jwt | cut -d. -f2 | base64 -d 2> /dev/null | sed 's/.*password":"//;s/",.*//;'
      echo ""
    fi
    
    # expire early
    if [ $1 == "exp" ]; then
      echo "Expiring JWT!"
      curl -s "https://apps.middleware.vt.edu/ws/v2/accounts/tokens" -XDELETE -H "Authorization: Bearer $(cat ~/.short-jwt)"
      rm ~/.short-jwt
    fi
}
