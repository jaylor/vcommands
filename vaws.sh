#!/bin/bash

export VAULT_ADDR="https://vault.es.cloud.vt.edu:8200/"

vaws_sts_set () {
  # Example usage:
  # vaws_sts_set <master-account> <account-to-be-managed> <role-name>
  # vaws_sts_set vtnis-ss 541585145005 NISAdmin
  unset AWS_ACCESS_KEY_ID
  unset AWS_SECRET_ACCESS_KEY
  unset AWS_SESSION_TOKEN
  AWS_ACCOUNT_NUMBER=$2
  AWS_ROLE_NAME=$3

  ttl=$(vault token lookup | grep ^ttl)
  if [ $? -ne 0 ]; then
     echo -e "\nVault token expired. Logging in to vault.\n" >&2
     vault login -method=ldap username=${USER}
  fi
  if [ -z "$AWS_STS_TIMEOUT" ]
  then
        AWS_STS_TIMEOUT=900
  fi

  export AWS_ACCESS_KEY_ID=$(vault kv get -field=id secret/${USER}/aws/$1)
  export AWS_SECRET_ACCESS_KEY=$(vault kv get -field=key secret/${USER}/aws/$1)

  token=$(aws sts assume-role --role-arn "arn:aws:iam::${AWS_ACCOUNT_NUMBER}:role/$AWS_ROLE_NAME" --role-session-name $USER-$AWS_ROLE_NAME-workstation --duration-seconds ${AWS_STS_TIMEOUT} )

  unset AWS_ACCESS_KEY_ID
  unset AWS_SECRET_ACCESS_KEY
  unset AWS_SESSION_TOKEN

  id=$(echo $token | jq --raw-output '.Credentials.AccessKeyId')
  key=$(echo $token | jq --raw-output '.Credentials.SecretAccessKey')
  session_token=$(echo $token | jq --raw-output '.Credentials.SessionToken')

  export AWS_ACCESS_KEY_ID=$id
  export AWS_SECRET_ACCESS_KEY=$key
  export AWS_SESSION_TOKEN=$session_token
}

vaws_do () {
  ttl=$(vault token lookup | grep ^ttl)
  if [ $? -ne 0 ]; then
     echo -e "\nVault token expired. Logging in to vault.\n" >&2
     vault login -method=ldap username=${USER}
  fi

  AWS_ACCESS_KEY_ID=$(vault kv get -field=id secret/${USER}/aws/$1) \
  AWS_SECRET_ACCESS_KEY=$(vault kv get -field=key secret/${USER}/aws/$1) \
  ${@:2}

}

vaws_sts_do () {
  # Example usage:
  # aws_sts_do <master-account> <account-to-be-managed> <role-name> <commands>
  # aws_sts_do vtnis-ss 541585145005 NISAdmin terraform plan

  ttl=$(vault token lookup | grep ^ttl)
  if [ $? -ne 0 ]; then
     echo -e "\nVault token expired. Logging in to vault.\n" >&2
     vault login -method=ldap username=${USER}
  fi

  if [ -z "$AWS_STS_TIMEOUT" ]
  then  
        AWS_STS_TIMEOUT=900
  fi

  AWS_ACCOUNT_NUMBER=$2
  AWS_ROLE_NAME=$3
  export AWS_ACCESS_KEY_ID=$(vault kv get -field=id secret/${USER}/aws/$1)
  export AWS_SECRET_ACCESS_KEY=$(vault kv get -field=key secret/${USER}/aws/$1)
  token=$(aws sts assume-role --role-arn "arn:aws:iam::${AWS_ACCOUNT_NUMBER}:role/${AWS_ROLE_NAME}" --role-session-name ${USER}-${AWS_ROLE_NAME}-workstation --duration-seconds ${AWS_STS_TIMEOUT} )
  unset AWS_ACCESS_KEY_ID
  unset AWS_SECRET_ACCESS_KEY
  unset AWS_SESSION_TOKEN

  id=$(echo $token | jq --raw-output '.Credentials.AccessKeyId')
  key=$(echo $token | jq --raw-output '.Credentials.SecretAccessKey')
  session_token=$(echo $token | jq --raw-output '.Credentials.SessionToken')

  AWS_ACCESS_KEY_ID=$id \
  AWS_SECRET_ACCESS_KEY=$key \
  AWS_SESSION_TOKEN=$session_token \
  ${@:4}

}
