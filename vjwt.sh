#!/bin/bash

export VAULT_ADDR="https://vault.es.cloud.vt.edu:8200/"

# This script pads base64 encoding for compatibility with some utilities
base64pad() {
    local base64_string=$1

    if [[ -z $base64_string ]]; then
        read base64_string
    fi

    # While String length modulo 4 is not equal to 0
    while [[ $((${#base64_string} % 4)) -ne 0 ]]; do
        base64_string="${base64_string}="
    done

    echo "$base64_string"
}

vjwt_checkout() {
       # check out new token and store in vault
       # FIXME: date -d doesn't work on MacOS, need to add a check, then set a manual tokenexpminutes
       tokenexp=$(date -d "$(vault token lookup | awk '/expire_time/ {print $2}')" +%s)
       datenow=$(date -u +%s)
       tokenexpminutes=$(($(($tokenexp-$datenow))/60))
       echo "JWT set to expire in $tokenexpminutes minutes"
       local token=$(curl -s "https://apps.middleware.vt.edu/ws/v2/accounts/tokens?validity=PT${tokenexpminutes}M" -XPOST -u "${JWT_PID:-$USER}")

       if [[ -n "$token" ]]; then
            if grep -q message <<<"$token"; then
                cat >&2 <<EOF

ERROR: No token received!

Server sent the following error content:

    $token
EOF

                return 1
            else
               tokenexp=$(date -d "$(vault token lookup | awk '/expire_time/ {print $2}')" +%s)
               vault kv put secret/${JWT_PID:-$USER}/jwt exp="$tokenexp" hash="$token"
            fi
        else
            echo 'ERROR: No token content received!' >&2
            return 1
        fi

    vault kv get -field=hash secret/${JWT_PID:-$USER}/jwt
}

vjwt_get() {
    # check vault token, and login if not valid
    ttl=$(vault token lookup | grep ^ttl)
    if [ $? -eq 0 ]; then
       tokenexp=$(vault kv get -field=exp secret/${JWT_PID:-$USER}/jwt)
       if [ -z ${tokenexp+x} ]; then
          tokenexp=$(date -d '-1 minutes' -u +%s)
       fi
       datenow=$(date -u +%s)
       if [ $datenow -lt $tokenexp ]; then
          vault kv get -field=hash secret/${JWT_PID:-$USER}/jwt
       else
          echo -e "\nJWT expired or unavaialable. Acquiring new JWT\n" >&2
          local token="$(vjwt_checkout)"
       fi
    else
       echo -e "\nVault token expired. Logging in to vault.\n" >&2
       vault login -method=ldap useranem=${JWT_PID:-$USER}
       vault token renew -increment=12h
       ttl=$(vault token lookup | grep ^ttl)
        if [ $? -eq 0 ]; then
           tokenexp=$(vault kv get -field=exp secret/${JWT_PID:-$USER}/jwt)
           if [ -z ${tokenexp+x} ]; then
              tokenexp=$(date -d '-1 minutes' -u +%s)
           fi
           datenow=$(date -u +%s)
           if [ $datenow -lt $tokenexp ]; then
              vault kv get -field=hash secret/${JWT_PID:-$USER}/jwt
           else
              echo -e "\nYou should expect a 2FA request via your default method after providing your password.\n\n NOTE: You must be on a VT network or on the All traffic VPN option!!!\n" >&2
              local token="$(vjwt_checkout)"
              printf $token
           fi
        fi
    fi
}

vjwt_getpassword() {
    local token="$(vjwt_get)"

    if [[ -n "$token" ]]; then
        echo "$token" | cut -d. -f2 | base64pad | base64 -d 2>/dev/null | sed 's/.*password":"//;s/",.*//;'
    fi
}

vjwt_copy() {
  if command -v pbcopy &> /dev/null
  then
    pbcopy <<<"$(vjwt_getpassword)"
  elif command -v xsel &> /dev/null
  then
    xsel -p <<<"$(vjwt_getpassword)"
  else
    echo "pbcopy/xsel command not found. use vjwt_getpassword to display the password to stdout"
  fi
}

vjwt_expire() {
    ttl=$(vault token lookup | grep ^ttl)
    if [ $? -eq 0 ]; then
        local response=$(curl -s "https://apps.middleware.vt.edu/ws/v2/accounts/tokens" -XDELETE -H "Authorization: Bearer $(vjwt_get)")

        if [[ -z "$response" ]] || grep -Eq 'Could not find token|JWT is expired' <<<"$response"; then
            # token successfully expired
            echo -e "\nJWT expired!\n"
            exptoken="$(date -d '-1 minutes' -u +%s)"
            vault kv put secret/jaylor/jwt exp="$exptoken"
        else
            if grep -q message <<<"$response"; then
                cat >&2 <<EOF

ERROR: Unable to expire token!

Server sent the following error content:

    $response
EOF

                return 1
            fi
        fi
    fi
}
