#!/bin/bash

export VAULT_ADDR="https://vault.es.cloud.vt.edu:8200/"

vvpn_all() {
    networkp="$(vault kv get -field=vpn secret/${JWT_PID:-$USER}/network)"
    vuser="${JWT_PID:-$USER}@vt.edu"
    printf "\n\nYou should expect a 2FA request via your default method in a moment.\n\nSudo password will be required by script. Please wait a moment... No need for "
    sudo -S <<< $(cat /sudo_password.txt) bash -c "{ printf \"$networkp\n\"; sleep 2; printf 'push\n'; } | sudo openconnect --protocol=pulse -b -q 'https://vpn.nis.vt.edu/alltraffic' --user $vuser --passwd-on-stdin"
}

vvpn_connect() {
    # check to see if vpn connection is already established
    vpncheck=$(ip a | grep tun)
    # check vault token, and login if not valid
    ttl=$(vault token lookup | grep ^ttl)
    if [ $? -eq 0 ]; then
       # check to see if vpn connection is already established
       vpncheck=$(ip a | grep tun)
       if [ $? -ne 0 ]; then
          vvpn_all
       else
          echo -e "\n\nVPN connection already established." >&2
       fi
    else
       echo -e "\nVault token expired. Logging in to vault.\n" >&2
       echo -e "\nYou should expect a 2FA request via your default method after providing your password." >&2
       vault login -method=ldap useranem=${JWT_PID:-$USER}
       vault token renew -increment=12h
       # check to see if vpn connection is already established
       vpncheck=$(ip a | grep tun)
       if [ $? -ne 0 ]; then
          vvpn_all
       else
          echo -e "\n\nVPN connection already established." >&2
       fi
    fi
}
